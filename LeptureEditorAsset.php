<?php
/**
 * @author Di Zhang <zhangdi_me@163.com>
 */

namespace yiizh\lepture\editor;

use yii\web\AssetBundle;

class LeptureEditorAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower/leptureeditor';

    public $css = [
        'editor.css',
        'paper.css',
        'vendor/icomoon/style.css'
    ];
    public $js = [
        'docs/marked.js',
        'src/intro.js',
        'vendor/codemirror.js',
        'vendor/markdown.js',
        'src/editor.js',

    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}